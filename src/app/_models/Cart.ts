import { ProductSelection } from './ProductSelection';

export class Cart {

  constructor(
    public items: ProductSelection[],
    public numberItems: number,
    public total: number
  ) { }
}

