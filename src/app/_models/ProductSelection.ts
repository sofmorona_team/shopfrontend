import {Product} from './Product';

export class ProductSelection {

  constructor(public product: Product,
              public quantity: number) {
  }
}

