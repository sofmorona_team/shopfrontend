import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { ProductService } from '../_services/product.service';
import { Product } from '../_models/Product';
import {ProductSelection} from '../_models/ProductSelection';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {

  public productsSelection: ProductSelection[];
  public totalProducts: number;
  public numPages: number;
  public actualPage: number;
  public limit: number;

  // It is to can send the selected product with its quantity to the shopping cart
  @Output() addToShoppingCart: EventEmitter<ProductSelection> = new EventEmitter<ProductSelection>();

  constructor(private productService: ProductService) { }

  ngOnInit() {
    this.actualPage = 0;
    this.limit = 10;
    this.loadProducts();
  }

  // To load all the products from the REST API
  loadProducts() {
    this.productService.getProducts(this.actualPage, this.limit).subscribe(
      response => {
        this.productsSelection = response.products.map( (product) => new ProductSelection(product, 1));
        this.totalProducts = response.total;
        this.numPages = Math.ceil(this.totalProducts / this.limit);
      },
      error => {
      }
    );
  }

  // Function to load a new page
  loadPage(page) {
    this.actualPage = page;
    this.loadProducts();
  }

  // Function to add the item to the chart list component
  addItem(item) {
    this.addToShoppingCart.emit(Object.assign({}, item));
  }
}
