import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ShoppingListComponent } from './shopping-list.component';
import { ProductSelectionComponent } from '../product-selection/product-selection.component';
import { ProductService } from '../_services/product.service';
import { HttpClientModule } from '@angular/common/http';

describe('ShoppingListComponent', () => {
  let component: ShoppingListComponent;
  let fixture: ComponentFixture<ShoppingListComponent>;
  let service: ProductService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      declarations: [ ShoppingListComponent, ProductSelectionComponent ],
      providers: [ProductService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShoppingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    service = TestBed.get(ProductService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
