import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductSelection } from './_models/ProductSelection';
import {CartListComponent} from './cart-list/cart-list.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {

  @ViewChild(CartListComponent) private cartList: CartListComponent;
  constructor() { }

  ngOnInit() {
  }

  sendItemToCart(productSelection: ProductSelection) {
    this.cartList.addItem(productSelection);
  }
}
