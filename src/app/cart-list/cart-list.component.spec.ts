import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CartListComponent } from './cart-list.component';
import {Product} from '../_models/Product';
import {ProductSelection} from '../_models/ProductSelection';
import {Cart} from '../_models/Cart';

describe('CartListComponent', () => {
  let component: CartListComponent;
  let fixture: ComponentFixture<CartListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should increment number of items in the cart - empty cart', () => {
    const product = new Product(1, 'test', '', '', 20.00);
    const productSelection = new ProductSelection(product, 1);
    const cart = new Cart([], 0, 0.00);

    component.cart = cart;
    component.increment(product);
    expect(component.cart).toEqual(new Cart([productSelection], 1, 20.00));
  });

  it('should increment number of items in the cart - no empty cart', () => {
    const product = new Product(1, 'test', '', '', 20.00);
    const productSelectioCart = new ProductSelection(product, 1);
    const cart = new Cart([productSelectioCart], 1, 20.00);
    const productSelectionFinal = new ProductSelection(product, 2);

    component.cart = cart;
    component.increment(product);
    expect(component.cart).toEqual(new Cart([productSelectionFinal], 2, 40.00));
  });

  it('should decrement the number of items in the cart', () => {
    const product = new Product(1, 'test', '', '', 20.00);
    const productSelectioCart = new ProductSelection(product, 2);
    const cart = new Cart([productSelectioCart], 2, 40.00);
    const productSelectionFinal = new ProductSelection(product, 1);

    component.cart = cart;
    component.decrement(product);
    expect(component.cart).toEqual(new Cart([productSelectionFinal], 1, 20.00));
  });

  it('should add new item', () => {
    const product = new Product(1, 'test', '', '', 20.00);
    const productSelection = new ProductSelection(product, 1);
    const cart = new Cart([], 0, 0.00);

    component.cart = cart;
    component.addItem(productSelection);
    expect(component.cart).toEqual(new Cart([productSelection], 1, 20.00));
  });

  it('should increment quantity existing item', () => {
    const product = new Product(1, 'test', '', '', 20.00);
    const productSelected = new ProductSelection(product, 1);
    const productInCart = new ProductSelection(product, 1);
    const cart = new Cart([productInCart], 1, 20.00);
    const productTotal = new ProductSelection(product, 2);

    component.cart = cart;
    component.addItem(productSelected);
    expect(component.cart).toEqual(new Cart([productTotal], 2, 40.00));
  });

  it('should remove item', () => {
    const product = new Product(1, 'test', '', '', 20.00);
    const productToRemove = new ProductSelection(product, 1);
    const productInCart = new ProductSelection(product, 2);
    const cart = new Cart([productInCart], 2, 40.00);
    const productSelectionFinal = new ProductSelection(product, 1);

    component.cart = cart;
    component.removeItem(productToRemove);
    expect(component.cart).toEqual(new Cart([productSelectionFinal], 1, 20.00));
  });

  it('should remove item but keep equal', () => {
    const product = new Product(1, 'test', '', '', 20.00);
    const productToRemove = new ProductSelection(product, 1);
    const productInCart = new ProductSelection(product, 1);
    const cart = new Cart([productInCart], 1, 20.00);
    const productSelectionFinal = new ProductSelection(product, 1);

    component.cart = cart;
    component.removeItem(productToRemove);
    expect(component.cart).toEqual(new Cart([productSelectionFinal], 1, 20.00));
  });

  it('should remove item from cart', () => {
    const product = new Product(1, 'test', '', '', 20.00);
    const productInCart = new ProductSelection(product, 1);
    const cart = new Cart([productInCart], 1, 20.00);
    const productSelectionFinal = new ProductSelection(product, 1);

    component.cart = cart;
    component.removeAllItem(productInCart);
    expect(component.cart).toEqual(new Cart([], 0, 0.00));
  });

  it('should reset cart', () => {
    const product = new Product(1, 'test', '', '', 20.00);
    const productSelection = new ProductSelection(product, 1);
    const cart = new Cart([productSelection], 1, 20.00);
    const emptyCart = new Cart([], 0, 0);

    component.cart = cart;
    component.buy();
    expect(component.cart).toEqual(emptyCart);
  });
});
