import { Component, OnInit } from '@angular/core';
import {ProductSelection} from '../_models/ProductSelection';
import {Cart} from '../_models/Cart';
import {Product} from '../_models/Product';

@Component({
  selector: 'app-cart-list',
  templateUrl: './cart-list.component.html',
  styleUrls: ['./cart-list.component.css']
})
export class CartListComponent implements OnInit {

  public cart: Cart;

  constructor( ) {
  }

  ngOnInit() {
    this.cart = new Cart([], 0, 0);
  }

  // Function to add an item from the list to the cart
  addItem(productSelection: ProductSelection) {
    // First we have to check if we have already the product in the cart
    const index = this.cart.items.findIndex( (item) => item.product.id === productSelection.product.id)
    if (index !== -1) {
      this.cart.items[index].quantity += productSelection.quantity;
    } else {
      this.cart.items.push(productSelection);
    }
    this.cart.total += productSelection.quantity * productSelection.product.price;
    this.cart.numberItems += productSelection.quantity;
  }

  // This function is to remove an element from the cart. It doesn't allow remove more elements than existing in the cart
  removeItem(productSelection: ProductSelection) {
    const index = this.cart.items.findIndex( (item) => item.product.id === productSelection.product.id)
    if (index !== -1) {
      if ( this.cart.items[index].quantity <= 1) {
        return false;
      }
      this.cart.total -= productSelection.quantity * productSelection.product.price;
      this.cart.numberItems -= productSelection.quantity;
      this.cart.items[index].quantity -= productSelection.quantity;
    }
  }

  // Function remove an item from the cart
  removeAllItem(productSelection: ProductSelection) {
    const index = this.cart.items.findIndex( (item) => item.product.id === productSelection.product.id);
    if (index !== -1) {
      this.cart.items.splice(index, 1);
      this.cart.total -= productSelection.quantity * productSelection.product.price;
      this.cart.numberItems -= productSelection.quantity;
    }
  }

  // Function to increment the number of items of the given product in the cart
  increment(product: Product) {
    const newProduct = new ProductSelection(product, 1);
    this.addItem(newProduct);
  }

  // Function to decrement the number of items of the given product in the cart
  decrement(product: Product) {
    const newProduct = new ProductSelection(product, 1);
    this.removeItem(newProduct);
  }

  // Function to reset the cart
  buy() {
    this.cart = new Cart([], 0, 0);
  }

}
