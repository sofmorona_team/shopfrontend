import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {ProductSelection} from '../_models/ProductSelection';

@Component({
  selector: 'app-product-selection',
  templateUrl: './product-selection.component.html',
  styleUrls: ['./product-selection.component.css']
})
export class ProductSelectionComponent implements OnInit {

  // Here we take the product from the list of products with which we are going to work
  public productSelection: ProductSelection;
  @Input() set productData(passedValue: ProductSelection) {
    this.productSelection = passedValue;
  }

  ngOnInit() {
  }

  removeElement() {
    if (this.productSelection.quantity > 1) {
      this.productSelection.quantity --;
    }
  }
  addElement() {
    this.productSelection.quantity++;
  }
}
