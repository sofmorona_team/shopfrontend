import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductSelectionComponent } from './product-selection.component';
import {Product} from '../_models/Product';
import {ProductSelection} from '../_models/ProductSelection';

describe('ProductSelectionComponent', () => {
  let component: ProductSelectionComponent;
  let fixture: ComponentFixture<ProductSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add element', () => {
    const product = new Product(1, 'test', '', '', 20.00);
    component.productSelection = new ProductSelection(product, 0);
    component.addElement();
    expect(component.productSelection.quantity).toEqual(1);
  });

  it('should remove element', () => {
    const product = new Product(1, 'test', '', '', 20.00);
    component.productSelection = new ProductSelection(product, 2);
    component.removeElement();
    expect(component.productSelection.quantity).toEqual(1);
  });

  it('should remove element but keep equal', () => {
    const product = new Product(1, 'test', '', '', 20.00);
    component.productSelection = new ProductSelection(product, 1);
    component.removeElement();
    expect(component.productSelection.quantity).toEqual(1);
  });

});
