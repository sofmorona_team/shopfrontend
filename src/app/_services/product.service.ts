import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { retry, catchError, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { environment } from '../../environments/environment';
import { Product } from '../_models/Product';

@Injectable()
export class ProductService {

  public apiUrl = environment.API_URL;

  constructor(
    private http: HttpClient
  ) { }

  /**
   * Function to get all Products from the API
   */
  getProducts(page: number, limit: number) {
    const url = this.apiUrl + 'products?page=' + page + '&limit=' + limit;
    return this.http.get(url).pipe(
      map((data) => ( {products: data['products'], total: data['total']} )),
      retry(1),
      catchError(this.handleError));
  }

  // Function to manage the error
  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

}
