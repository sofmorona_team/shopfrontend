import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { CartListComponent } from './cart-list/cart-list.component';
import { ProductSelectionComponent } from './product-selection/product-selection.component';

import { ProductService } from './_services/product.service';

@NgModule({
  declarations: [
    AppComponent,
    ShoppingListComponent,
    CartListComponent,
    ProductSelectionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [ProductService],
  bootstrap: [AppComponent]
})
export class AppModule { }
