# ShopFrontend

Web application to display a set of products and allow the user to add them to a shopping cart.

## Environment

The application use a REST API on http://localhost:8080.
To modify it the file src/enviroments/environment.prod.ts need to be changed

## Start

Run `npm install`. 
Run `npm run init`.
